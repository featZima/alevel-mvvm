package com.featzima.mvvm;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Single;

public class NamesRepositoryIml implements NamesRepository {

    @Inject
    public NamesRepositoryIml() {
    }

    @Override
    public Single<Boolean> isBeautifulName(final String userName) {
        return Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Thread.sleep(3000);
                return userName.startsWith("D");
            }
        });
    }
}
