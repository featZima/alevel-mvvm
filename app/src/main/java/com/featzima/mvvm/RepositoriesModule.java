package com.featzima.mvvm;

import dagger.Binds;
import dagger.Module;

@Module
public interface RepositoriesModule {

    @Binds
    NamesRepository provideNamesRepository(NamesRepositoryIml repository);


}
