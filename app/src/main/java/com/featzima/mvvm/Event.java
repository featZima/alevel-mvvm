package com.featzima.mvvm;

public class Event<T> {
    T data;
    private boolean isHandled;

    public Event(T data) {
        this.data = data;
        this.isHandled = false;
    }

    public void setHandled(boolean handled) {
        isHandled = handled;
    }

    public boolean isHandled() {
        return isHandled;
    }
}
