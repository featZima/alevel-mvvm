package com.featzima.mvvm;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class MainViewModelFactory implements ViewModelProvider.Factory {

    private final MainViewModel mainViewModel;

    @Inject
    public MainViewModelFactory(MainViewModel mainViewModel) {
        this.mainViewModel = mainViewModel;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) mainViewModel;
    }
}
