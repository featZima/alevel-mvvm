package com.featzima.mvvm;

import dagger.Component;
import dagger.android.AndroidInjector;

@Component(modules = {
        UiModule.class,
        RepositoriesModule.class
})
public interface ApplicationComponent extends AndroidInjector<Application> {

}
