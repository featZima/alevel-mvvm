package com.featzima.mvvm;

import io.reactivex.Single;

public interface NamesRepository {

    Single<Boolean> isBeautifulName(String userName);
}
