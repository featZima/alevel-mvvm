package com.featzima.mvvm;

import android.widget.SeekBar;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

public class SeekBarBindingAdapter {

    @BindingAdapter("userProgress")
    public static void setUserProgress(SeekBar seekBar, Integer userProgress) {
        if (userProgress != null) {
            if (seekBar.getProgress() != userProgress) {
                seekBar.setProgress(userProgress);
            }
        } else {
            if (seekBar.getProgress() != 0) {
                seekBar.setProgress(0);
            }
        }
    }


    @BindingAdapter("userProgressAttrChanged")
    public static void setProgressAttrChanged(SeekBar seekBar, final InverseBindingListener listener) {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                listener.onChange();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @InverseBindingAdapter(attribute = "userProgress")
    public static Integer getProgress(SeekBar seekBar) {
        return seekBar.getProgress();
    }

}
