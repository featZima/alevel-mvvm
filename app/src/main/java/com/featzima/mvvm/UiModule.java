package com.featzima.mvvm;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = {AndroidSupportInjectionModule.class})
public abstract class UiModule {

    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

}
