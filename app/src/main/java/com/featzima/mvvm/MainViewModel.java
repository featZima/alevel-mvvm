package com.featzima.mvvm;

import android.os.AsyncTask;

import javax.inject.Inject;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    public MutableLiveData<Integer> counter = new MutableLiveData<>();
    public LiveData<Boolean> isShowGreeting = Transformations.map(counter, new Function<Integer, Boolean>() {
        @Override
        public Boolean apply(Integer input) {
            return input < 10;
        }
    });
    public MutableLiveData<Integer> progress = new MutableLiveData<>();
    public MutableLiveData<String> userName = new MutableLiveData<>();
    MutableLiveData<Event<String>> error = new MutableLiveData<>();
    public MutableLiveData<Boolean> isValid = new MutableLiveData<>();
    public MutableLiveData<Boolean> isBeautifulName = new MutableLiveData<>();
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private final NamesRepository namesRepository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public MainViewModel(NamesRepository namesRepository) {
        super();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                while(true) {
                    try {
                        Thread.sleep(1000);
                        int previousValue = 0;
                        if (counter.getValue() != null) {
                            previousValue = counter.getValue();
                        };
                        counter.postValue(previousValue + 1);
                        if (previousValue == 3) {
                            error.postValue(new Event<>("Third tick"));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
        this.namesRepository = namesRepository;
        isLoading.postValue(false);
        userName.postValue("Dmytro");
        userName.observeForever(new Observer<String>() {
            @Override
            public void onChanged(String userName) {
                isValid.postValue(userName.length() > 0);
            }
        });
        userName.observeForever(new Observer<String>() {
            @Override
            public void onChanged(String userName) {
                Disposable disposable = MainViewModel.this.namesRepository
                        .isBeautifulName(userName)
                        .doOnSubscribe(new Consumer<Disposable>() {
                            @Override
                            public void accept(Disposable disposable) throws Exception {
                                isLoading.postValue(true);
                            }
                        })
                        .doOnSuccess(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean aBoolean) throws Exception {
                                isLoading.postValue(false);
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<Boolean>() {
                            @Override
                            public void accept(Boolean isBeautifulName) throws Exception {
                                MainViewModel.this.isBeautifulName.postValue(isBeautifulName);
                            }
                        });
                compositeDisposable.add(disposable);
            }
        });
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }

    public void resetCounter() {
        counter.postValue(0);
    }

    public void changeName(String newUserName) {
        userName.postValue(newUserName);
    }
}
