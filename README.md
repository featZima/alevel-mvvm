Data Binding библиотека
=======================

Ресурсы
-------
[Официальная документация](https://developer.android.com/topic/libraries/data-binding/)

Подключение
-----------
Добавить в файл `app/build.gradle`:
```
...
android {
    ...
    dataBinding {
        enabled = true
    }
}
```

Пример использования
--------------------
`MainViewModel.java`:
```java
public class MainViewModel extends ViewModel {
    public MutableLiveData<String> userName = new MutableLiveData<>();
    public MutableLiveData<String> isReady = Transformations.map(userName, new Function<Integer, Boolean>() {
         @Override
         public Boolean apply(String userName) {
             return userName.length > 0;
         }
    });
}
```

`main_layout.xml`:
```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="android.view.View" />
        <variable
            name="viewModel"
            type="com.featzima.mvvm.MainViewModel" />
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">

        <EditText
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:text="@={viewModel.userName}" />

        <Button
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:visible="@{viewModel.isReady ? View.VISIBLE : View.GONE}"/>

    </LinearLayout>
<layout>
```

`MainActivity.java`:
```java
public class MainActivity extends DaggerAppCompatActivity {

    private MainViewModel viewModel;
    @Inject MainViewModelFactory mainViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.viewModel = ViewModelProviders
                .of(this, mainViewModelFactory)
                .get(MainViewModel.class);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
    }
}
```